import React, { ReactElement } from "react";
import Navigator from "@config/navigator";
import { SettingProvider } from "@contexts/settings-context";
import Amplify from "aws-amplify";
import awsExports from "./aws-exports";
import { AuthProvider } from "@contexts/auth-context";
import { AppBootstrap } from "@components";

Amplify.configure(awsExports);

export default function App(): ReactElement {
  return (
    <AuthProvider>
      <AppBootstrap>
        <SettingProvider>
          <Navigator />
        </SettingProvider>
      </AppBootstrap>
    </AuthProvider>
  );
}
