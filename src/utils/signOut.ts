import Constants from "expo-constants";
import * as Notifications from "expo-notifications";
import { Auth, API, graphqlOperation } from "aws-amplify";
import gql from "graphql-tag";

const signOut = async (): Promise<void> => {
  // if (Constants.isDevice) {
  //   const tokenRes = await Notifications.getExpoPushTokenAsync();
  //   console.log(tokenRes);
  //   const deleteExpoToken = gql`
  //     mutation deleteExpoToken($token: String!) {
  //       deleteExpoToken(input: { token: $token }) {
  //         token
  //       }
  //     }
  //   `;
  //   try {
  //     await API.graphql(
  //       graphqlOperation(deleteExpoToken, {
  //         token: tokenRes.data,
  //       })
  //     );
  //   } catch (error) {
  //     console.log("error:", error);
  //   }
  // }
  await Auth.signOut();
};

export default signOut;
