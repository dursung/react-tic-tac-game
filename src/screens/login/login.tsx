import React, { ReactElement, useRef, useState } from "react";
import {
  ScrollView,
  TextInput as NativeTextInput,
  TouchableOpacity,
} from "react-native";
import { GradientBackground, TextInput, Button, Text } from "@components";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackNavigatorParams } from "@config/navigator";
import { RouteProp } from "@react-navigation/native";
import styles from "./login.styles";
import { Auth } from "aws-amplify";

type LoginProps = {
  navigation: StackNavigationProp<StackNavigatorParams, "Login">;
  route: RouteProp<StackNavigatorParams, "Login">;
};

export default function Login({ navigation, route }: LoginProps): ReactElement {
  const redirect = route.params?.redirect;
  const passwordRef = useRef<NativeTextInput | null>(null);
  const [form, setForm] = useState({ username: "", password: "" });
  const [loading, setLoading] = useState(false);

  const setFormInput = (key: keyof typeof form, value: string) => {
    setForm({ ...form, [key]: value });
  };

  // const singup = async () => {
  //   try {
  //     const res = await Auth.signUp({
  //       username: "test",
  //       password: "12345678",
  //       attributes: {
  //         email: "test@test.com",
  //         name: "Test Test",
  //       },
  //     });
  //     console.log(res);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const login = async () => {
    setLoading(true);
    const { username, password } = form;
    try {
      const res = await Auth.signIn(username, password);
      console.log("res: " + JSON.stringify(res));
      //navigation.navigate("Home");
      redirect ? navigation.replace(redirect) : navigation.navigate("Home");
    } catch (error) {
      if (error.code === "UserNotConfirmedException") {
        navigation.navigate("SignUp", { username });
      }
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <GradientBackground>
      <ScrollView contentContainerStyle={styles.container}>
        <TextInput
          value={form.username}
          onChangeText={(value) => {
            setFormInput("username", value);
          }}
          placeholder="Username"
          style={{ marginBottom: 20 }}
          returnKeyType="next"
          onSubmitEditing={() => {
            passwordRef.current?.focus();
          }}
        />
        <TextInput
          value={form.password}
          onChangeText={(value) => {
            setFormInput("password", value);
          }}
          style={{ marginBottom: 40 }}
          ref={passwordRef}
          returnKeyType="done"
          placeholder="Password"
          secureTextEntry
        />
        <Button title="Login" onPress={login} loading={loading} />
        <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
          <Text style={styles.registerLink}>Don&apos;t have an account?</Text>
        </TouchableOpacity>
      </ScrollView>
    </GradientBackground>
  );
}
