import React, { ReactElement, useState } from "react";
import { Image, ScrollView, View, TouchableOpacity, Alert } from "react-native";
import styles from "./home.syles";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackNavigatorParams } from "@config/navigator";
import { GradientBackground, Button, Text } from "@components";
import { IMAGENAME } from "../../../assets";
import { useAuth } from "@contexts/auth-context";
import { signOut } from "@utils";

type HomeProps = {
  navigation: StackNavigationProp<StackNavigatorParams, "Home">;
};

export default function Home({ navigation }: HomeProps): ReactElement {
  const { user, setUser } = useAuth();
  const [signingOut, setSigingOut] = useState(false);
  return (
    <GradientBackground>
      <ScrollView contentContainerStyle={styles.container}>
        <Image style={styles.logo} source={IMAGENAME} />
        <View style={styles.buttons}>
          <Button
            onPress={() => {
              navigation.navigate("SinglePlayerGame");
            }}
            style={styles.button}
            title="Single Player"
          />
          <Button
            style={styles.button}
            title="Multiplayer"
            onPress={() => {
              if (user) {
                navigation.navigate("MultiplayerHome");
              } else {
                navigation.navigate("Login", { redirect: "MultiplayerHome" });
              }
            }}
          />
          <Button
            loading={signingOut}
            onPress={async () => {
              if (user) {
                setSigingOut(true);
                try {
                  await signOut();
                } catch (error) {
                  Alert.alert("Error!", "Error signing out!");
                }
                setSigingOut(false);
              } else {
                navigation.navigate("Login");
              }
            }}
            style={styles.button}
            title={user ? "Logout" : "Login"}
          />
          <Button
            onPress={() => {
              navigation.navigate("Settings");
            }}
            style={styles.button}
            title="Settings"
          />

          {user && (
            <Text weight="400" style={styles.loggedInText}>
              Logged in as <Text weight="700">{user.username}</Text>
            </Text>
          )}
        </View>
      </ScrollView>
    </GradientBackground>
  );
}
