import { colors } from "@utils";
import React, { ReactElement, forwardRef } from "react";
import {
  TextInput as NativeTextInput,
  StyleSheet,
  TextInputProps as NativeTextInputProps,
} from "react-native";

const styles = StyleSheet.create({
  input: {
    height: 50,
    width: "100%",
    borderBottomWidth: 1,
    borderColor: colors.lightGreen,
    backgroundColor: colors.purple,
    padding: 10,
    color: colors.lightGreen,
    fontFamily: "DeliusUnicase_400Regular",
  },
});

const TextInput = forwardRef<NativeTextInput, NativeTextInputProps>(
  ({ style, ...props }, ref): ReactElement => {
    return (
      <NativeTextInput
        ref={ref}
        {...props}
        placeholderTextColor="#5379"
        style={[styles.input, style]}
      />
    );
  }
);
TextInput.displayName = "TextInput";

export default TextInput;
