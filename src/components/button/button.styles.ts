import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  button: {
    backgroundColor: "#dafaf7",
    paddingVertical: 20,
    paddingHorizontal: 35,
    justifyContent: "center",
    borderRadius: 30,
  },
  buttonText: {
    fontSize: 18,
    alignItems: "center",
    justifyContent: "center",
    color: "#221a36",
  },
});

export default styles;
