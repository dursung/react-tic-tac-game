/* Amplify Params - DO NOT EDIT
	API_TICTACTOE_GRAPHQLAPIENDPOINTOUTPUT
	API_TICTACTOE_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const appsync = require("aws-appsync");
const gql = require("graphql-tag");
require("cross-fetch/polyfill");
const AUTH_TYPE = require("aws-appsync").AUTH_TYPE;

exports.handler = async (event, context, callback) => {
  const graphqlClient = new appsync.AWSAppSyncClient({
    url: process.env.API_TICTACTOE_GRAPHQLAPIENDPOINTOUTPUT,
    region: process.env.REGION,
    auth: {
      type: AUTH_TYPE.AWS_IAM,
      credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        sessionToken: process.env.AWS_SESSION_TOKEN,
      },
    },
    disableOffline: true,
  });

  const query = gql`
    query getPlayer($username: String!) {
      getPlayer(username: $username) {
        id
      }
    }
  `;

  const mutation = gql`
    mutation createPlayer(
      $name: String!
      $cognitoID: String!
      $username: String!
      $email: AWSEmail!
    ) {
      createPlayer(
        input: {
          cognitoID: $cognitoID
          email: $email
          username: $username
          name: $name
        }
      ) {
        id
      }
    }
  `;

  try {
    const response = await graphqlClient.query({
      query,
      variables: {
        username: event.userName,
      },
    });
    if (response.data.getPlayer) {
      callback(null, event);
    } else {
      try {
        await graphqlClient.mutate({
          mutation,
          variables: {
            name: event.request.userAttributes.name,
            username: event.userName,
            cognitoID: event.request.userAttributes.sub,
            email: event.request.userAttributes.email,
          },
        });
        callback(null, event);
      } catch (error) {
        callback(error);
      }
    }
  } catch (error) {
    callback(error);
  }
};
